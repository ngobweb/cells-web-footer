var templateBind = document.getElementById('tbind');

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  // auto binding template is ready
  templateBind.set('greeting', 'Try declarative!');
});

document.addEventListener('WebComponentsReady', function() {
  var myEl = document.querySelector('.demo');
  // set component properties here
    var logo = 'banking:B51';
    var slogan = 'icons:A17';
    var links = [
      {
        text: 'Security Center',
        href: '#',
        target: '_blank',
      },
      {
        text: 'Privacy',
        href: '#',
        target: '_blank',
      },
      {
        text: 'Terms',
        href: '#',
        target: '_blank',
      },
      {
        text: 'Site Map',
        href: '#',
        target: '_blank',
      }
    ];
    var rss = [
      {
        text: '',
        target: '_blank',
        href: '#',
        icon: {
                code: 'banking:B29',
                size: '21'
        }
      },
      {
        target: '_blank',
        href: '#',
        icon: {
                code: 'banking:B29',
                size: '21'
        }
      },
      {
        target: '_blank',
        href: '#',
        icon: {
                code: 'banking:B29',
                size: '21'
        }
      },
      {
        target: '_blank',
        href: '#',
        icon: {
                code: 'banking:B29',
                size: '21'
        }
      },
      {
        target: '_blank',
        href: '#',
        icon: {
                code: 'banking:B29',
                size: '21'
        }
      },
      {
        target: '_blank',
        href: '#',
        icon: {
                code: 'banking:B29',
                size: '21'
        }
      },
      {
        target: '_blank',
        href: '#',
        icon: {
                code: 'banking:B29',
                size: '21'
        }
      }
    ];
    myEl.set('logo', logo);
    myEl.set('slogan', slogan);
    myEl.set('rss', rss);
    myEl.set('links', links);
});
