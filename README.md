# cells-web-footer

Your component description.

Example:
```html
<cells-web-footer></cells-web-footer>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-web-footer-scope      | scope description | default value  |
| --cells-web-footer  | empty mixin     | {}             |
