(function() {
  'use strict';

  Polymer({

    is: 'cells-web-footer',

    behaviors: [
      Polymer.i18nBehavior
    ],

    properties: {
      /**
      * Footer
      * @type {Object}
      */
      footer: {
        type: Object,
        observer: '_setFooter'
      },
      /**
      * Logo
      * @type {String}
      */
      logo: {
        type: String
      },
      /**
      * Slogan of logo
      * @type {String}
      */
      slogan: {
        type: String
      },
      /**
      * List links of helpers
      * @type {Array}
      */
      links: {
        type: Array,
        value: function() {
          return [];
        }
      },
      /**
      * List social network
      * @type {Array}
      */
      rss: {
        type: Array,
        value: function() {
          return [];
        }
      },
      /**
      * Is mobile version
      * @type {Boolean}
      */
      isMobile: {
        type: Boolean,
        value: false
      },
      /**
      * Mediaquery
      * @type {Boolean}
      */
      query: {
        type: String,
        value: '(max-width: 48rem)'
      }
    },
    /*
    * Set footer
    */
    _setFooter: function(footer) {
      var logo = footer.logo || undefined;
      var slogan = footer.slogan || undefined;
      var links = footer.links || [];
      var rss = footer.rss || [];
      this.set('logo', logo);
      this.set('slogan', slogan);
      this.set('links', links);
      this.set('rss', rss);
    },
    /*
    * Determines if is isMobile set class isMobile
    */
    _isMobile: function(isMobile) {
      return (isMobile) ? 'mobile' : '';
    }
  });
}());
